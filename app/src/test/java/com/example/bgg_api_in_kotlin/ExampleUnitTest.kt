package com.example.bgg_api_in_kotlin

import com.example.bgg_api_in_kotlin.dataModel.Artist
import com.example.bgg_api_in_kotlin.dataModel.BoardGame
import com.example.bgg_api_in_kotlin.dataModel.relations.ArtistWithGames
import org.junit.Test
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.bgg_api_in_kotlin.dataModel.Location
import com.example.bgg_api_in_kotlin.dbHelper.LocationDao
import kotlinx.coroutines.runBlocking
import org.junit.After

import org.junit.Assert.*
import org.junit.Before
import org.junit.runner.RunWith

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }
}

@Database(entities = [Location::class], version = 1)
abstract class TestDatabase : RoomDatabase() {
    abstract fun locationDao(): LocationDao
}

@RunWith(AndroidJUnit4::class)
class LocationDaoTest {

    private lateinit var db: TestDatabase
    private lateinit var dao: LocationDao

    @Before
    fun initDb() {
        db = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            TestDatabase::class.java
        ).build()
        dao = db.locationDao()
    }

    @After
    fun closeDb() {
        db.close()
    }

    @Test
    fun insertAndReadLocation() = runBlocking {
        val location = Location(id = 0, name = "Location1", description = "Description1")
        dao.addLocation(location)
        val locations = dao.getLocation()
        assertEquals(1, locations.size)
        assertEquals("Location1", locations[0].name)
    }

    @Test
    fun updateAndReadLocation() = runBlocking {
        val location = Location(id = 0, name = "Location1", description = "Description1")
        dao.addLocation(location)
        val updatedLocation =
            Location(id = 1, name = "UpdatedLocation1", description = "UpdatedDescription1")
        dao.updateLocation(updatedLocation)
        val locationFromDb = dao.getLocation(1)
        assertEquals("UpdatedLocation1", locationFromDb.name)
        assertEquals("UpdatedDescription1", locationFromDb.description)
    }

    @Test
    fun deleteLocation() = runBlocking {
        val location = Location(id = 0, name = "Location1", description = "Description1")
        dao.addLocation(location)
        dao.deleteLocation(1)
        val locations = dao.getLocation()
        assertTrue(locations.isEmpty())
    }
}

