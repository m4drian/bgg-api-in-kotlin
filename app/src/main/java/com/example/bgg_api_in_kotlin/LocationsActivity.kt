package com.example.bgg_api_in_kotlin

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bgg_api_in_kotlin.components.RecyclerLocAdapter
import com.example.bgg_api_in_kotlin.components.RecyclerLocation
import com.example.bgg_api_in_kotlin.dataModel.Location
import com.example.bgg_api_in_kotlin.dbHelper.BoardGameDao
import com.example.bgg_api_in_kotlin.dbHelper.BoardGameDatabase
import com.example.bgg_api_in_kotlin.dbHelper.BoardGameViewModel
import com.example.bgg_api_in_kotlin.dbHelper.LocationDao
import kotlinx.coroutines.launch

import android.widget.EditText

import android.content.DialogInterface
import android.view.View


class LocationsActivity : AppCompatActivity() {


    private lateinit var viewModelBgg: BoardGameViewModel
    private lateinit var daoBgg : BoardGameDao
    private lateinit var daoLocation : LocationDao

    private lateinit var recyclerViewItems : List<RecyclerLocation>
    private lateinit var recyclerLocations : RecyclerView

    lateinit var locList : List<Location>
    private var adapter : RecyclerLocAdapter = RecyclerLocAdapter()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_locations)

        val year : Int = intent.getIntExtra("year",2020)
        val buttonOk = findViewById<Button>(R.id.buttonOkLocations)
        val buttonAdd = findViewById<Button>(R.id.buttonAddLocations)

        recyclerLocations = findViewById<RecyclerView>(R.id.recyclerLocations)

        lifecycleScope.launch {
            //viewModelBgg = ViewModelProvider(this@LocationsActivity).get(BoardGameViewModel::class.java)
            //daoBgg = BoardGameDatabase.getDatabase(this@LocationsActivity).boardGameDao()
            daoLocation = BoardGameDatabase.getDatabase(this@LocationsActivity).locationDao()

            //daoLocation.addLocation(Location(1,"Biblioteka","No fajnie"))
            //daoBgg.updateGame(BoardGame(1,-1,"","","W BIBLIO","W BIBLIO","",1999,-1,"",null,null,null,null,1))
            locList = daoLocation.getLocation()

            //val recyclerViewItems : List<RecyclerLocation>
            if(locList.isNotEmpty()) {
                recyclerViewItems = generateRecyclerViewItems(locList)
            }
            else
            {
                recyclerViewItems = generateRecyclerViewItems(null)
            }

            //adapter = RecyclerLocAdapter()
            adapter.updateData(recyclerViewItems)
            recyclerLocations.adapter = adapter
            recyclerLocations.layoutManager = LinearLayoutManager(this@LocationsActivity)
            //recyclerLocations.setHasFixedSize(true)

        }

        val intentEdit = Intent(this@LocationsActivity,EditLocActivity::class.java)


        adapter.setOnItemClickListener(object : RecyclerLocAdapter.OnItemClickListener {

            //short click edit
            override fun onItemClick(position: Int) {

                if(!recyclerViewItems[position].text0.isNullOrBlank()) {
                    intentEdit.putExtra("id", recyclerViewItems[position].text0.toInt())
                    Handler(Looper.getMainLooper()).postDelayed({
                        startActivity(intentEdit)
                        finish()
                    }, 100)
                }
            }

            //long click delete
            override fun onLongItemClick(position: Int) {
                lifecycleScope.launch {

                    if(!daoLocation.isLocationWithBoardGames(locList[position].id)) {
                        daoLocation.deleteLocation(locList[position].id)

                        locList = daoLocation.getLocation()


                        val recyclerViewItems: List<RecyclerLocation>
                        if (locList.isNotEmpty()) {
                            recyclerViewItems = generateRecyclerViewItems(locList)
                        } else {
                            recyclerViewItems = generateRecyclerViewItems(null)
                        }

                        adapter.updateData(recyclerViewItems)

                        Toast.makeText(this@LocationsActivity, "Location deleted", Toast.LENGTH_SHORT).show()

                    }
                    else
                    {
                        Toast.makeText(this@LocationsActivity, "Location isn't empty", Toast.LENGTH_SHORT).show()
                    }
                }
            }

        })

        buttonOk.setOnClickListener{
            Handler(Looper.getMainLooper()).postDelayed({
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            },100)
        }

        buttonAdd.setOnClickListener{

            showAlertAddLoc()

        }


    }

    private fun generateRecyclerViewItems(locs:List<Location>?): List<RecyclerLocation> {
        val list = ArrayList<RecyclerLocation>()

        if(!locs.isNullOrEmpty())
        {

            locs.forEach{
                list += RecyclerLocation(it.id.toString(),it.name.toString(),it.description.toString())
            }

        }
        else{
            var item = RecyclerLocation("","no locations yet", "")
            list += item
        }

        return list
    }

    private fun showAlertAddLoc() {

        // Create an alert builder
        val builder: AlertDialog.Builder = android.app.AlertDialog.Builder(this@LocationsActivity)
        builder.setTitle("Name")

        // set the custom layout
        val customLayout: View = layoutInflater
            .inflate(
                R.layout.prompt_location,
                null
            )
        builder.setView(customLayout)

        // add a button
        builder
            .setPositiveButton(
                "OK",
                DialogInterface.OnClickListener { dialog, which -> // send data from the
                    // AlertDialog to the Activity
                    val editText: EditText = customLayout
                        .findViewById(
                            R.id.prompt1
                        )

                    val editText2: EditText = customLayout
                        .findViewById(
                            R.id.prompt2
                        )

                    lifecycleScope.launch{
                        if(!editText.text.toString().isNullOrBlank()) {

                            daoLocation.addLocation(
                                Location(
                                    0,
                                    editText.text.toString(),
                                    editText2.text.toString()
                                )
                            )
                            locList = daoLocation.getLocation()
                            val recyclerViewItems : List<RecyclerLocation>
                            if(locList.isNotEmpty()) {recyclerViewItems = generateRecyclerViewItems(locList)}
                            else {recyclerViewItems = generateRecyclerViewItems(null)}
                            adapter.updateData(recyclerViewItems)

                            Toast.makeText(
                                this@LocationsActivity,
                                "Location added",
                                Toast.LENGTH_SHORT
                            ).show()

                        }
                        else{
                            Toast.makeText(
                                this@LocationsActivity,
                                "Location needs a name!",
                                Toast.LENGTH_SHORT
                            ).show()
                        }


                    }

                })

        // create and show
        // the alert dialog
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }
}
