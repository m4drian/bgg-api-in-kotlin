package com.example.bgg_api_in_kotlin.dbHelper

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.example.bgg_api_in_kotlin.dataModel.Location
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LocationViewModel(application: Application):AndroidViewModel(application) {

    //private val readAllData : LiveData<List<Location>>
    private val repository: LocationRepository
    init {
        val locationDao = BoardGameDatabase.getDatabase(application).locationDao()
        repository = LocationRepository(locationDao)
        //readAllData = repository.readAllData
    }

    fun addLocation(location:Location){
        viewModelScope.launch(Dispatchers.IO) {
            repository.addLocation(location)
        }
    }
}