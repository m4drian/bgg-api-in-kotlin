package com.example.bgg_api_in_kotlin.dbHelper

import com.example.bgg_api_in_kotlin.dataModel.Artist
import com.example.bgg_api_in_kotlin.dataModel.BoardGame
import com.example.bgg_api_in_kotlin.dataModel.Designer
import com.example.bgg_api_in_kotlin.dataModel.RankHistory
import com.example.bgg_api_in_kotlin.dataModel.relations.BoardGameArtistCrossRef
import com.example.bgg_api_in_kotlin.dataModel.relations.BoardGameDesignerCrossRef

class BoardGameRepository(private val boardGameDao: BoardGameDao) {

    //val readGameData: List<BoardGame> = boardGameDao.readGameData()

    suspend fun addBoardGame(boardGame: BoardGame){
        boardGameDao.addBoardGame(boardGame)
    }

    suspend fun addArtist(artist: Artist){
        boardGameDao.addArtist(artist)
    }

    suspend fun addDesigner(designer: Designer){
        boardGameDao.addDesigner(designer)
    }

    suspend fun addRank(rank: RankHistory){
        boardGameDao.addRank(rank)
    }

    suspend fun addArtistGameCrossRef(artref: BoardGameArtistCrossRef){
        boardGameDao.addArtistGameCrossRef(artref)
    }

    suspend fun addDesignerGameCrossRef(desref: BoardGameDesignerCrossRef){
        boardGameDao.addDesignerGameCrossRef(desref)
    }
}
