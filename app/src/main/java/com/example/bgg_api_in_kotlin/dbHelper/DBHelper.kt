package com.example.bgg_api_in_kotlin.dbHelper

import android.content.ContentValues
import android.content.Context;
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.example.bgg_api_in_kotlin.dataModel.PlayerScore

//deprecated
class DBHelper(context:Context):SQLiteOpenHelper(context,DATABASE_NAME, null,DATABASE_VER) {

    companion object{
        private val DATABASE_VER = 1
        private val DATABASE_NAME = "UBI2DB.db"

        //
        private val TABLE_NAME="PlayerScore"
        //private val TABLE_NAME2="Login"//will require separate helper
        private val COL_ID="Id"
        private val COL_NAME="Name"
        private val COL_SCORE="Score"

    }

    override fun onCreate(db: SQLiteDatabase?) {
        val CREATE_TABLE_QUERY:String = ("CREATE TABLE $TABLE_NAME ($COL_ID INTEGER PRIMARY KEY AUTOINCREMENT,$COL_NAME TEXT,$COL_SCORE INTEGER)")
        db!!.execSQL(CREATE_TABLE_QUERY)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
       db!!.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(db)
    }

    //
    val allPlayer:List<PlayerScore>
        get(){
            val lstPlayers = ArrayList<PlayerScore>()
            val selectQuery = "SELECT * FROM $TABLE_NAME"
            val db:SQLiteDatabase = this.writableDatabase
            val cursor = db.rawQuery(selectQuery,null)

            if(cursor.moveToFirst())
            {
                do{
                    val player = PlayerScore()
                    player.id = cursor.getInt(cursor.getColumnIndex(COL_ID))
                    player.name = cursor.getString(cursor.getColumnIndex(COL_NAME))
                    player.score = cursor.getInt(cursor.getColumnIndex(COL_SCORE))

                    lstPlayers.add(player)
                }while(cursor.moveToNext())
            }

            cursor.close()
            return lstPlayers
        }

    fun addPlayer(player:PlayerScore)
    {
        val db:SQLiteDatabase = this.writableDatabase
        val values = ContentValues()
        //values.put(COL_ID,player.id)
        values.put(COL_NAME,player.name)
        values.put(COL_SCORE,player.score)

        db.insert(TABLE_NAME,null,values)
        db.close()
    }

    fun updatePlayer(player:PlayerScore):Int{
        val db:SQLiteDatabase = this.writableDatabase
        val values = ContentValues()
        values.put(COL_ID,player.id)
        values.put(COL_NAME,player.name)
        values.put(COL_SCORE,player.score)

        return db.update(TABLE_NAME,values,"$COL_ID=?",arrayOf(player.id.toString()))
    }

    fun deletePlayer(player:PlayerScore){
        val db:SQLiteDatabase = this.writableDatabase
        db.delete(TABLE_NAME,"$COL_ID=?",arrayOf(player.id.toString()))
        db.close()
    }

}