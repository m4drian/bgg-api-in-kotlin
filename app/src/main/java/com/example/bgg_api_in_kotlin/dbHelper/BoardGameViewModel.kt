package com.example.bgg_api_in_kotlin.dbHelper

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.example.bgg_api_in_kotlin.dataModel.Artist
import com.example.bgg_api_in_kotlin.dataModel.BoardGame
import com.example.bgg_api_in_kotlin.dataModel.Designer
import com.example.bgg_api_in_kotlin.dataModel.RankHistory
import com.example.bgg_api_in_kotlin.dataModel.relations.BoardGameArtistCrossRef
import com.example.bgg_api_in_kotlin.dataModel.relations.BoardGameDesignerCrossRef
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class BoardGameViewModel(application: Application):AndroidViewModel(application) {

    //private val readGameData : LiveData<List<BoardGame>>
    private val repository: BoardGameRepository
    init {
        val boardGameDao = BoardGameDatabase.getDatabase(application).boardGameDao()
        repository = BoardGameRepository(boardGameDao)
        //readGameData = repository.readGameData
    }

    fun addBoardGame(boardGame:BoardGame){
        viewModelScope.launch(Dispatchers.IO) {
            repository.addBoardGame(boardGame)
        }
    }

    fun addArtist(artist: Artist){
        viewModelScope.launch(Dispatchers.IO) {
            repository.addArtist(artist)
        }
    }

    fun addDesigner(designer: Designer){
        viewModelScope.launch(Dispatchers.IO) {
            repository.addDesigner(designer)
        }
    }

    fun addRank(rank: RankHistory){
        viewModelScope.launch(Dispatchers.IO) {
            repository.addRank(rank)
        }
    }

    fun addArtistGameCrossRef(artref: BoardGameArtistCrossRef){
        viewModelScope.launch(Dispatchers.IO) {
            repository.addArtistGameCrossRef(artref)
        }
    }

    fun addDesignerGameCrossRef(desref: BoardGameDesignerCrossRef){
        viewModelScope.launch(Dispatchers.IO) {
            repository.addDesignerGameCrossRef(desref)
        }
    }
}