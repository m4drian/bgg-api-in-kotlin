package com.example.bgg_api_in_kotlin.dbHelper

import androidx.room.*
import com.example.bgg_api_in_kotlin.dataModel.BoardGame
import com.example.bgg_api_in_kotlin.dataModel.Location

@Dao
interface LocationDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addLocation(location: Location)

    @Query("SELECT * FROM location_table ORDER BY id ASC")
    suspend fun getLocation(): List<Location>

    @Query("SELECT * FROM location_table WHERE id = :locId LIMIT 1")
    suspend fun getLocation(locId : Int): Location

    @Update
    suspend fun updateLocation(location : Location)

    @Query("DELETE FROM location_table WHERE id = :locId")
    suspend fun deleteLocation(locId: Int)

    @Transaction
    @Query("SELECT EXISTS(SELECT * FROM board_game_table WHERE locationId = :locId)")
    suspend fun isLocationWithBoardGames(locId : Int) : Boolean

    @Transaction
    @Query("SELECT * FROM board_game_table WHERE locationId = :locationId LIMIT 1")
    suspend fun getLocationWithBoardGames(locationId: Int): List<BoardGame>

}