package com.example.bgg_api_in_kotlin.dbHelper

import androidx.room.*
import com.example.bgg_api_in_kotlin.dataModel.Artist
import com.example.bgg_api_in_kotlin.dataModel.BoardGame
import com.example.bgg_api_in_kotlin.dataModel.Designer
import com.example.bgg_api_in_kotlin.dataModel.RankHistory
import com.example.bgg_api_in_kotlin.dataModel.relations.*

import androidx.room.Delete

import androidx.room.Update




@Dao
interface BoardGameDao {

    //Board Game
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addBoardGame(boardGame: BoardGame)

    @Query("SELECT EXISTS(SELECT * FROM board_game_table WHERE bggId = :bggId)")
    suspend fun isGameExists(bggId : Int) : Boolean

    @Query("SELECT EXISTS(SELECT * FROM board_game_table)")
    suspend fun isAnyGameExists() : Boolean

    @Query("UPDATE board_game_table SET rank=:rank WHERE bggId = :bggId")
    suspend fun updateGameRank(rank: Int, bggId: Int)

    @Query("DELETE FROM board_game_table WHERE gameId = :gameId")
    suspend fun deleteGameWhereId(gameId: Int)

    @Update//
    suspend fun updateGame(game: BoardGame)

    @Transaction
    @Query("UPDATE board_game_table SET name = :name , originalName = :ogname , description = :desc , yearPublished = :yr , comment = :com , buy = :buy , suggestedPrice = :suggestedPrice , eanUpcCode = :eanUpcCode , productionCode = :productionCode , locationId = :locationId WHERE gameId = :gameId")
    suspend fun updateGameWithId(gameId: Int, name:String, ogname: String , desc : String, yr:Int, com:String, buy:String, suggestedPrice:String, eanUpcCode:String, productionCode:String , locationId:Int)

    @Transaction
    @Query("UPDATE board_game_table SET name = :txt WHERE gameId = :gameId")
    suspend fun updateGameName(gameId: Int, txt:String)
    @Transaction
    @Query("UPDATE board_game_table SET originalName = :txt WHERE gameId = :gameId")
    suspend fun updateGameOgName(gameId: Int, txt:String)
    @Transaction
    @Query("UPDATE board_game_table SET description = :txt WHERE gameId = :gameId")
    suspend fun updateGameDesc(gameId: Int, txt:String)
    @Transaction
    @Query("UPDATE board_game_table SET yearPublished = :num WHERE gameId = :gameId")
    suspend fun updateGameYear(gameId: Int, num:Int)
    @Transaction
    @Query("UPDATE board_game_table SET comment = :txt WHERE gameId = :gameId")
    suspend fun updateGameCom(gameId: Int, txt:String)
    @Transaction
    @Query("UPDATE board_game_table SET buy = :txt WHERE gameId = :gameId")
    suspend fun updateGameBuy(gameId: Int, txt:String)
    @Transaction
    @Query("UPDATE board_game_table SET suggestedPrice = :txt WHERE gameId = :gameId")
    suspend fun updateGameSuggested(gameId: Int, txt:String)
    //eanUpcCode = :eanUpcCode , productionCode = :productionCode , locationId = :locationId
    @Transaction
    @Query("UPDATE board_game_table SET eanUpcCode = :txt WHERE gameId = :gameId")
    suspend fun updateGameUpc(gameId: Int, txt:String)
    @Transaction
    @Query("UPDATE board_game_table SET productionCode = :txt WHERE gameId = :gameId")
    suspend fun updateGameProduction(gameId: Int, txt:String)
    @Transaction
    @Query("UPDATE board_game_table SET locationId = :num WHERE gameId = :gameId")
    suspend fun updateGameLocation(gameId: Int, num:Int)

    @Delete//
    suspend fun deleteGame(game: BoardGame)

    @Transaction
    @Query("SELECT * FROM board_game_table ORDER BY gameId ASC")
    suspend fun getGameData(): List<BoardGame>

    @Transaction
    @Query("SELECT * FROM board_game_table ORDER BY name ASC")
    suspend fun getGameDataNameAsc(): List<BoardGame>
    @Transaction
    @Query("SELECT * FROM board_game_table ORDER BY name DESC")
    suspend fun getGameDataNameDesc(): List<BoardGame>
    @Transaction
    @Query("SELECT * FROM board_game_table ORDER BY (CASE WHEN yearPublished = -1 THEN 1 ELSE 0 END) ASC,yearPublished ASC")
    suspend fun getGameDataYearAsc(): List<BoardGame>
    @Transaction
    @Query("SELECT * FROM board_game_table ORDER BY yearPublished DESC")
    suspend fun getGameDataYearDesc(): List<BoardGame>
    @Transaction
    @Query("SELECT * FROM board_game_table ORDER BY (CASE WHEN rank = -1 THEN 1 ELSE 0 END) ASC,rank ASC")
    suspend fun getGameDataRankAsc(): List<BoardGame>
    @Transaction
    @Query("SELECT * FROM board_game_table ORDER BY rank DESC")
    suspend fun getGameDataRankDesc(): List<BoardGame>

    @Transaction
    @Query("SELECT * FROM board_game_table WHERE bggId = :bggId LIMIT 1")
    suspend fun getGameWithBggid(bggId : Int): BoardGame?

    @Transaction
    @Query("SELECT * FROM board_game_table WHERE gameId = :gameId LIMIT 1")
    suspend fun getGameWithId(gameId : Int): BoardGame?

    @Transaction
    @Query("SELECT * FROM board_game_table WHERE gameId = :gameId")
    suspend fun getBoardGameWithRanks(gameId: Int): List<BoardGameWithRankHistory>

    //Artist
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addArtist(artist: Artist)

    @Transaction
    @Query("DELETE FROM artist_crossref WHERE gameId = :gameId")
    suspend fun deleteArtistCrossRef(gameId: Int)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addArtistGameCrossRef(crossRef: BoardGameArtistCrossRef)

    @Transaction
    @Query("SELECT * FROM artist_table ORDER BY name ASC")
    suspend fun getArtist(): List<Artist>

    @Transaction
    @Query("SELECT * FROM artist_table WHERE name = :name")
    suspend fun getGameOfArtist(name: String): List<ArtistWithGames>

    @Transaction
    @Query("SELECT * FROM board_game_table WHERE gameId = :gameId")
    suspend fun getArtistOfGame(gameId: Int): List<GameWithArtists>


    //Designer
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addDesigner(designer: Designer)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addDesignerGameCrossRef(crossRef: BoardGameDesignerCrossRef)

    @Transaction
    @Query("DELETE FROM designer_crossref WHERE gameId = :gameId")
    suspend fun deleteDesignerCrossRef(gameId: Int)

    @Transaction
    @Query("SELECT * FROM designer_table ORDER BY name ASC")
    suspend fun getDesigner(): List<Designer>

    @Transaction
    @Query("SELECT * FROM designer_table WHERE name = :name")
    suspend fun getGameOfDesigner(name: String): List<DesignerWithGames>

    @Transaction
    @Query("SELECT * FROM board_game_table WHERE bggId = :bggId")
    suspend fun getDesignerOfGame(bggId: Int): List<GameWithDesigners>

    //Rank History
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addRank(rankHistory: RankHistory)

    @Transaction
    @Query("SELECT * FROM rank_history_table ORDER BY id ASC")
    suspend fun getRank(): List<RankHistory>

    @Query("DELETE FROM rank_history_table WHERE bggId = :bggId")
    suspend fun deleteRankWhereBggid(bggId: Int)

    @Query("SELECT * FROM rank_history_table WHERE bggId = :bggId ORDER BY date ASC")
    suspend fun getRankById(bggId : Int) : List<RankHistory>?

    @Query("SELECT EXISTS(SELECT * FROM board_game_table WHERE bggId = :bggId)")
    suspend fun isRankExists(bggId : Int) : Boolean
}