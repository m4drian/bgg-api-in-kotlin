package com.example.bgg_api_in_kotlin.dbHelper

import com.example.bgg_api_in_kotlin.dataModel.Location

class LocationRepository(private val locationDao: LocationDao) {

    //val readAllData: LiveData<List<Location>> = locationDao.getLocation()

    suspend fun addLocation(location: Location){
        locationDao.addLocation(location)
    }
}
