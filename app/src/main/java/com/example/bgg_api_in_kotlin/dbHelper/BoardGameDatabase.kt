package com.example.bgg_api_in_kotlin.dbHelper

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.bgg_api_in_kotlin.dataModel.*
import com.example.bgg_api_in_kotlin.dataModel.relations.BoardGameArtistCrossRef
import com.example.bgg_api_in_kotlin.dataModel.relations.BoardGameDesignerCrossRef

@Database(
    entities = [
        Artist::class,
        Designer::class,
        Location::class,
        RankHistory::class,
        BoardGame::class,
        BoardGameArtistCrossRef::class,
        BoardGameDesignerCrossRef::class
               ],
    version = 10,
    exportSchema = false
)
abstract class BoardGameDatabase : RoomDatabase() {

    abstract fun locationDao() : LocationDao
    abstract fun boardGameDao() : BoardGameDao

    companion object {
        @Volatile
        private var INSTANCE: BoardGameDatabase? = null

        fun getDatabase(context: Context) : BoardGameDatabase{
            val tempInstance = INSTANCE
            if(tempInstance != null){
                return tempInstance
            }
            synchronized(this){
                val instance = Room.databaseBuilder(
                context.applicationContext,
                BoardGameDatabase::class.java,
                "bgg_database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}