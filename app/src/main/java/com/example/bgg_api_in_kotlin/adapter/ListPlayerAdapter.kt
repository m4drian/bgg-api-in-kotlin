package com.example.bgg_api_in_kotlin.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.example.bgg_api_in_kotlin.dataModel.PlayerScore
import com.example.bgg_api_in_kotlin.R
import kotlinx.android.synthetic.main.recycler_item2.view.*

class ListPlayerAdapter(
    activity: Activity,
    private var lstPlayer:List<PlayerScore>
                        //internal var edt_id:EditText,
                        //internal var edt_name:TextView,
                        //internal var edt_score:TextView
                        ):BaseAdapter() {

    //internal var inflater:LayoutInflater
    private var inflater:LayoutInflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    @SuppressLint("ViewHolder", "InflateParams")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val rowView:View = inflater.inflate(R.layout.recycler_item2,null)

        rowView.txt_loc1.text = lstPlayer[position].name.toString()
        rowView.txt_loc2.text = lstPlayer[position].score.toString()

        /*rowView.setOnClickListener{
            edt_name.setText(rowView.txt_Name.toString())
            edt_score.setText(rowView.txt_Score.toString())
        }*/

        return rowView
    }

    override fun getItem(position: Int): Any {
        return lstPlayer[position]
    }

    override fun getItemId(position: Int): Long {
        return lstPlayer[position].id.toLong()
    }

    override fun getCount(): Int {
        return lstPlayer.size
    }

}