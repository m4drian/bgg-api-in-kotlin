package com.example.bgg_api_in_kotlin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bgg_api_in_kotlin.components.RecyclerEditLocAdapter
import com.example.bgg_api_in_kotlin.components.RecyclerLocation
import com.example.bgg_api_in_kotlin.dataModel.BoardGame
import com.example.bgg_api_in_kotlin.dataModel.Location
import com.example.bgg_api_in_kotlin.dbHelper.BoardGameDatabase
import com.example.bgg_api_in_kotlin.dbHelper.LocationDao
import kotlinx.coroutines.launch

class EditLocActivity : AppCompatActivity() {

    private lateinit var daoLocation : LocationDao

    private lateinit var recyclerViewItems : List<RecyclerLocation>
    private lateinit var recyclerLocations : RecyclerView

    lateinit var gameList : List<BoardGame>
    private var adapter : RecyclerEditLocAdapter = RecyclerEditLocAdapter()

    var locId : Int = -1

    //adapter.updateData(recyclerViewItems)
    //recyclerLocations.adapter = adapter
    //recyclerLocations.layoutManager = LinearLayoutManager(this@LocationsActivity)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_loc)

        val buttonBack = findViewById<Button>(R.id.buttonBackLoc)
        val buttonEdit = findViewById<Button>(R.id.buttonEditLoc)
        val txtName = findViewById<EditText>(R.id.locEdit1)
        val txtDesc = findViewById<EditText>(R.id.locEdit2)
        recyclerLocations = findViewById<RecyclerView>(R.id.recyclerLocations)

        locId = intent.getIntExtra("id",-1)

        lifecycleScope.launch() {
            daoLocation = BoardGameDatabase.getDatabase(this@EditLocActivity).locationDao()

            if (locId != -1){
                val thisLoc = daoLocation.getLocation(locId)

                txtName.setText(thisLoc.name)
                txtDesc.setText(thisLoc.description)

                val gameList = daoLocation.getLocationWithBoardGames(locId)
                if(gameList.isNotEmpty()) {
                    recyclerViewItems = generateRecyclerViewItems(gameList)
                }
                else
                {
                    recyclerViewItems = generateRecyclerViewItems(null)
                }

                adapter.updateData(recyclerViewItems)
                recyclerLocations.adapter = adapter
                recyclerLocations.layoutManager = LinearLayoutManager(this@EditLocActivity)
                //Toast.makeText(this@EditLocActivity,locId.toString(),Toast.LENGTH_SHORT).show()

            }
        }

        buttonEdit.setOnClickListener{

            val txtName = findViewById<EditText>(R.id.locEdit1)
            val txtDesc = findViewById<EditText>(R.id.locEdit2)

            lifecycleScope.launch() {
                daoLocation = BoardGameDatabase.getDatabase(this@EditLocActivity).locationDao()
                daoLocation.updateLocation(Location(locId,txtName.text.toString(),txtDesc.text.toString()))

            }

            Toast.makeText(this@EditLocActivity,"location with id ${locId.toString()} updated",Toast.LENGTH_SHORT).show()
        }

        buttonBack.setOnClickListener{
            Handler(Looper.getMainLooper()).postDelayed({
                startActivity(Intent(this, LocationsActivity::class.java))
                finish()
            },100)
        }
    }

    private fun generateRecyclerViewItems(games:List<BoardGame>?): List<RecyclerLocation> {
        val list = ArrayList<RecyclerLocation>()

        if(!games.isNullOrEmpty())
        {

            games.forEach{
                var name : String = ""
                if(it.name.toString().isBlank())
                {
                    name = it.originalName.toString()
                }
                else
                {
                    name = it.name.toString()
                }

                list += RecyclerLocation("id:${it.gameId.toString()}",name,"desc:${it.description.toString()}")
            }

        }
        else{
            var item = RecyclerLocation("","no games here", "")
            list += item
        }

        return list
    }


}