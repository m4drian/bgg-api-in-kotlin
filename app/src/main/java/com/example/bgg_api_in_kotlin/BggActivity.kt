package com.example.bgg_api_in_kotlin

import android.content.Intent
import android.icu.text.SimpleDateFormat
import android.icu.util.Calendar
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Button
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.example.bgg_api_in_kotlin.components.MyPreference
import com.example.bgg_api_in_kotlin.dataModel.BoardGame
import com.example.bgg_api_in_kotlin.dataModel.RankHistory
import com.example.bgg_api_in_kotlin.dataModel.XmlItem
import com.example.bgg_api_in_kotlin.dbHelper.BoardGameDao
import com.example.bgg_api_in_kotlin.dbHelper.BoardGameDatabase
import com.example.bgg_api_in_kotlin.dbHelper.BoardGameViewModel
import kotlinx.android.synthetic.main.activity_bgg.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.ArrayList

class BggActivity : AppCompatActivity() {

    private lateinit var viewModelBgg:BoardGameViewModel

    private lateinit var daoBgg : BoardGameDao

    private lateinit var xmlList : List<XmlItem>

    private lateinit var bgs : ArrayList<BoardGame>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bgg)

        val buttonOk = findViewById<Button>(R.id.buttonOkBgg)
        val buttonConfirm = findViewById<Button>(R.id.buttonConfirmBgg)
        val buttonUpdate = findViewById<Button>(R.id.buttonUpdateBgg)

        val myPref = MyPreference(this)
        var prefNick = myPref.getNick()

        bgg_text.setText(prefNick)

        buttonOk.setOnClickListener{
            Handler(Looper.getMainLooper()).postDelayed({
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            },100)
        }

        buttonConfirm.setOnClickListener{
            if(prefNick != bgg_text.text.toString()) {
                myPref.setNick(bgg_text.text.toString())
                prefNick = bgg_text.text.toString()
            }

            Toast.makeText(this@BggActivity, "Please wait, if nothing happens try again", Toast.LENGTH_LONG).show()

            val collection = "https://www.boardgamegeek.com/xmlapi2/collection?username=${prefNick}&stats=1"
            val xmlTask = MainActivity.DownloadXml( this@BggActivity,collection, 2)

            GlobalScope.launch( context = Dispatchers.Main){
                xmlTask.execute()
            }

            bgs = ArrayList<BoardGame>();

            GlobalScope.launch( context = Dispatchers.IO){
                xmlList = xmlTask.get()
                lifecycleScope.launch{
                    viewModelBgg = ViewModelProvider(this@BggActivity).get(BoardGameViewModel::class.java)
                    daoBgg = BoardGameDatabase.getDatabase(this@BggActivity).boardGameDao()
                    //viewModelBgg = ViewModelProvider(this@BggActivity).get(BoardGameViewModel::class.java)


                    xmlList.forEach{
                        val isRank = it.rank
                        val tRank : Int = if(isRank!=null) {
                            if(isRank.isNotBlank() && isRank.toIntOrNull()!=null)
                                isRank.toInt()
                            else
                                -1
                        } else {
                            -1
                        }

                        bgs.add(BoardGame(
                            0,
                            it.bggid,
                            it.type,
                            it.thumbnail,
                            it.name,
                            it.originalName,
                            it.description,
                            it.yearPublished,
                            tRank,
                            it.comment,
                            null,
                            null,
                            null,
                            null,
                            null))
                        }

                    if(bgs.first().bggId != -1) {
                        val date = Calendar.getInstance().time
                        val strDate = SimpleDateFormat("yyyy/MM/dd HH:mm", Locale.getDefault()).format(date)
                        bgs.forEach {
                            if(!daoBgg.isGameExists(it.bggId)) {
                                viewModelBgg.addBoardGame(it)//TODO:if not add then update?

                                val rnk = it.rank ?: -1
                                if(rnk!=-1) {
                                    viewModelBgg.addRank(RankHistory(0, strDate, rnk, it.bggId))
                                }

                            }
                            else
                            {
                                val rnk = it.rank ?: -1
                                //val oldRnk = daoBgg.getGame(it.bggId).rank ?: -1
                                //if(rnk!=-1 && oldRnk != rnk) {
                                //    viewModelBgg.addRank(RankHistory(0, strDate, rnk, it.originalName,it.bggId))
                                //    daoBgg.updateGameRank(rnk,it.bggId)
                                //}
                                val oldRnk : Int
                                val game = daoBgg.getGameWithBggid(it.bggId)
                                if(game != null)
                                {
                                    oldRnk = game.rank ?: -2
                                }
                                else
                                {
                                    oldRnk = -2
                                }

                                if(rnk!=-1 && oldRnk != rnk && oldRnk != -2) {
                                    viewModelBgg.addRank(RankHistory(0, strDate, rnk, it.bggId))
                                    daoBgg.updateGameRank(rnk,it.bggId)
                                }
                            }

                        }
                        Toast.makeText(this@BggActivity, "Successfully added/updated ${bgs.size} games", Toast.LENGTH_SHORT).show()
                    }



                }
            }


        }

        buttonUpdate.setOnClickListener{
            lifecycleScope.launch {
                //viewModelBgg =
                //    ViewModelProvider(this@BggActivity).get(BoardGameViewModel::class.java)
                daoBgg = BoardGameDatabase.getDatabase(this@BggActivity).boardGameDao()

                val date = Calendar.getInstance().time
                val strDate = SimpleDateFormat("yyyy/MM/dd HH:mm", Locale.getDefault()).format(date)

                val collection = "https://www.boardgamegeek.com/xmlapi2/collection?username=${prefNick}&stats=1"
                val xmlTask = MainActivity.DownloadXml( this@BggActivity,collection, 2)

                xmlTask.execute()
                xmlList = xmlTask.get()

                xmlList.forEach {
                    val isRank = it.rank
                    val rnk : Int = if(isRank!=null) {
                        if(isRank.isNotBlank() && isRank.toIntOrNull()!=null)
                            isRank.toInt()
                        else
                            -1
                    } else {
                        -1
                    }

                    val oldRnk : Int
                    val game = daoBgg.getGameWithBggid(it.bggid)
                    if(game != null)
                    {
                        oldRnk = game.rank ?: -2
                    }
                    else
                    {
                        oldRnk = -2
                    }

                    if(rnk!=-1 && oldRnk != rnk && oldRnk != -2) {
                        viewModelBgg.addRank(RankHistory(0, strDate, rnk, it.bggid))
                         daoBgg.updateGameRank(rnk,it.bggid)
                    }

                }

                Toast.makeText(this@BggActivity, "Rankings updated on " + strDate, Toast.LENGTH_SHORT).show()



            }


        }
    }
}