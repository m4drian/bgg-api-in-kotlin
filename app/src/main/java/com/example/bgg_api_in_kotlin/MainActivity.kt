package com.example.bgg_api_in_kotlin

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.GestureDetector
import android.view.MotionEvent
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.view.GestureDetectorCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bgg_api_in_kotlin.components.*
import com.example.bgg_api_in_kotlin.dataModel.Artist
import com.example.bgg_api_in_kotlin.dataModel.BoardGame
import com.example.bgg_api_in_kotlin.dataModel.XmlItem
import com.example.bgg_api_in_kotlin.dbHelper.BoardGameDao
import com.example.bgg_api_in_kotlin.dbHelper.BoardGameDatabase
import com.example.bgg_api_in_kotlin.dbHelper.BoardGameViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.xmlpull.v1.XmlPullParserException
import java.io.IOException
import java.lang.Math.abs


class MainActivity : AppCompatActivity() {

    private lateinit var detector : GestureDetectorCompat

    private lateinit var viewModelBgg:BoardGameViewModel
    private lateinit var daoBgg : BoardGameDao

    lateinit var gameList : List<BoardGame>
    private var adapter : RecyclerAdapter = RecyclerAdapter()

    private lateinit var recyclerViewItems : List<RecyclerItem>
    private lateinit var recyclerGames : RecyclerView

    var sortName : Boolean = false
    var sortDate : Boolean = false
    var sortRank : Boolean = false

    private val permissions = arrayOf(
        android.Manifest.permission.INTERNET,
        android.Manifest.permission.ACCESS_NETWORK_STATE,
        android.Manifest.permission.WRITE_EXTERNAL_STORAGE
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val myPref = MyPreference(this)

        detector = GestureDetectorCompat(this,GestureListener())

        if (!hasPermissions(this, *permissions)) {
            ActivityCompat.requestPermissions(this, permissions, 1)
        }

        recyclerGames = findViewById<RecyclerView>(R.id.recyclerMain)

        lifecycleScope.launch {
            viewModelBgg = ViewModelProvider(this@MainActivity).get(BoardGameViewModel::class.java)
            daoBgg = BoardGameDatabase.getDatabase(this@MainActivity).boardGameDao()

            if(daoBgg.isAnyGameExists())
            {

                gameList = daoBgg.getGameData()
                recyclerViewItems = generateRecyclerViewItems(gameList)

                adapter.updateData(recyclerViewItems)
                recyclerGames.adapter = adapter
                recyclerGames.layoutManager = LinearLayoutManager(this@MainActivity)
                recyclerGames.setHasFixedSize(true)
            }

        }

        val intentDetails = Intent(this@MainActivity,GameDetailsActivity::class.java)
        val intentLocations = Intent(this@MainActivity,LocationsActivity::class.java)
        //intentHandlowe.putExtra("year", datyDefault)

        adapter.setOnItemClickListener(object : RecyclerAdapter.OnItemClickListener {
            //short click edit
            override fun onItemClick(position: Int) {
                    intentDetails.putExtra("gameid", recyclerViewItems[position].id)

                    Handler(Looper.getMainLooper()).postDelayed({
                        startActivity(intentDetails)
                        finish()
                    }, 100)
            }
            //long click delete
            override fun onLongItemClick(position: Int) {

                lifecycleScope.launch{
                    daoBgg = BoardGameDatabase.getDatabase(this@MainActivity).boardGameDao()

                    val game = daoBgg.getGameWithId(recyclerViewItems[position].id)
                    if(game != null) {
                        daoBgg.deleteRankWhereBggid(game.bggId)
                        daoBgg.deleteArtistCrossRef(game.bggId)
                        daoBgg.deleteDesignerCrossRef(game.bggId)
                        daoBgg.deleteGameWhereId(game.gameId)

                        gameList = daoBgg.getGameDataNameAsc()
                        recyclerViewItems = generateRecyclerViewItems(gameList)
                        adapter.updateData(recyclerViewItems)


                        Toast.makeText(this@MainActivity, "Game deleted", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        })

        btnSortName.setOnClickListener{
            lifecycleScope.launch{
                daoBgg = BoardGameDatabase.getDatabase(this@MainActivity).boardGameDao()

                if(sortName == false) {
                    sortName = true
                    gameList = daoBgg.getGameDataNameAsc()
                    recyclerViewItems = generateRecyclerViewItems(gameList)
                    adapter.updateData(recyclerViewItems)

                }
                else
                {
                    sortName = false
                    gameList = daoBgg.getGameDataNameDesc()
                    recyclerViewItems = generateRecyclerViewItems(gameList)
                    adapter.updateData(recyclerViewItems)

                }


            }
        }

        btnSortDate.setOnClickListener{
            lifecycleScope.launch{
                daoBgg = BoardGameDatabase.getDatabase(this@MainActivity).boardGameDao()

                if(sortDate == false) {
                    sortDate = true
                    gameList = daoBgg.getGameDataYearAsc()
                    recyclerViewItems = generateRecyclerViewItems(gameList)
                    adapter.updateData(recyclerViewItems)

                }
                else
                {
                    sortDate = false
                    gameList = daoBgg.getGameDataYearDesc()
                    recyclerViewItems = generateRecyclerViewItems(gameList)
                    adapter.updateData(recyclerViewItems)

                }

            }
        }

        btnSortRank.setOnClickListener{
            lifecycleScope.launch{
                daoBgg = BoardGameDatabase.getDatabase(this@MainActivity).boardGameDao()

                if(sortName == false) {
                    sortName = true
                    gameList = daoBgg.getGameDataRankAsc()
                    recyclerViewItems = generateRecyclerViewItems(gameList)
                    adapter.updateData(recyclerViewItems)

                }
                else
                {
                    sortName = false
                    gameList = daoBgg.getGameDataRankDesc()
                    recyclerViewItems = generateRecyclerViewItems(gameList)
                    adapter.updateData(recyclerViewItems)

                }

            }
        }

        buttonLocations.setOnClickListener{
            Handler(Looper.getMainLooper()).postDelayed({
                startActivity(intentLocations)
                finish()
            },100)
        }

        buttonAddGame.setOnClickListener{
            Handler(Looper.getMainLooper()).postDelayed({
                startActivity(Intent(this@MainActivity, AddGameActivity::class.java))
                finish()
            },100)
        }

        buttonBGG.setOnClickListener{
            Handler(Looper.getMainLooper()).postDelayed({
                startActivity(Intent(this@MainActivity, BggActivity::class.java))
                finish()
            },100)
        }

    }

    private fun hasPermissions(context: Context, vararg permissions: String): Boolean = permissions.all {
        ActivityCompat.checkSelfPermission(context, it) == PackageManager.PERMISSION_GRANTED
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        return if(detector.onTouchEvent(event))
        {
            true
        }else{
            super.onTouchEvent(event)
        }
    }

    inner class GestureListener: GestureDetector.SimpleOnGestureListener(){
        private val SWIPE_TRESHOLD = 400
        private val SWIPE_VELOCITY_TRESHOLD = 400

        override fun onFling(
            downEv: MotionEvent?,
            moveEv: MotionEvent?,
            velocityX: Float,
            velocityY: Float
        ): Boolean {
            val diffX = moveEv?.x?.minus(downEv!!.x) ?: 0.0F
            val diffY = moveEv?.y?.minus(downEv!!.y) ?: 0.0F

            if(abs(diffX) <= abs(diffY))//up or down
            {
                if(abs(diffY) > SWIPE_TRESHOLD && abs(velocityY) > SWIPE_VELOCITY_TRESHOLD)
                {
                    if(diffY > 0)//up
                    {
                        //xmlGet()
                        //insertArtistToDatabase()
                    }
                }
            }
            else
            {
                //left or right
            }

            return super.onFling(downEv, moveEv, velocityX, velocityY)
        }
    }

    private fun insertArtistToDatabase() {

        lifecycleScope.launch {

            viewModelBgg = ViewModelProvider(this@MainActivity).get(BoardGameViewModel::class.java)
            daoBgg = BoardGameDatabase.getDatabase(this@MainActivity).boardGameDao()

            val art = Artist("Mep")
            viewModelBgg.addArtist(art)
            //val a = viewModelBgg.getArtist()

            val a = daoBgg.getArtist()
            if (a.isNotEmpty()) {
                Toast.makeText(this@MainActivity, "${a.first().name}", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(this@MainActivity, "CiaoCiao", Toast.LENGTH_SHORT).show()
            }
        }

    }

    private fun xmlGet(){
        if(hasPermissions(this@MainActivity, *permissions)) {
            val search = "https://www.boardgamegeek.com/xmlapi2/search?query=saboteur&type=boardgame"
            val collection = "https://www.boardgamegeek.com/xmlapi2/collection?username=jerichoinruins&stats=1"
            //jerichoinruins / rahdo
            val details = "https://www.boardgamegeek.com/xmlapi2/thing?id=102794&stats=1"

            var list : List<XmlItem> = listOf(XmlItem())

            val xmlTask = DownloadXml(this@MainActivity,collection,2)

            GlobalScope.launch( context = Dispatchers.Main){
                xmlTask.execute()
            }

            GlobalScope.launch( context = Dispatchers.Main){
                list = xmlTask.get()
                val item : XmlItem = list.last()
                messageThis(this@MainActivity,"${item.bggid}, ${item.originalName}, ${item.artist}\n${item.type},${item.rank}")
            }

        }
    }

    //--------------------------------------------which:1-search,2-collection,else-details
    @SuppressLint("StaticFieldLeak")//private var activity: Activity,
    class DownloadXml(var activity: Activity, val url: String, val which: Int?) : AsyncTask<String, Void, List<XmlItem>>() {

        override fun doInBackground(vararg params: String?): List<XmlItem> {
            return try {
                val parser = XmlParser()
                parser.loadXmlFromNetwork(url,which)
            } catch (e: IOException) {
                listOf(XmlItem(activity.resources.getString(R.string.connection_error)))
            } catch (e: XmlPullParserException) {
                listOf(XmlItem(activity.resources.getString(R.string.xml_error)))
            }
        }

        override fun onPostExecute(result: List<XmlItem>) {
            //MessageThis(activity,result)
            /*setContentView(R.layout.main)
            // Displays the HTML string in the UI via a WebView
            findViewById<WebView>(R.id.webview)?.apply {
                loadData(result, "text/html", null)
            }*/
            return
        }
    }

    private fun messageThis(activity: Activity, msg:String){
        val test = AlertDialog.Builder(activity);//applicationContext
        //test.setMessage("cool");
        test.setMessage(msg);
        test.setPositiveButton(android.R.string.yes) { dialog, which ->
            Toast.makeText(
                applicationContext,
                android.R.string.ok, Toast.LENGTH_SHORT
            ).show()
        }
        test.show()
    }

    private fun generateRecyclerViewItems(games : List<BoardGame>): List<RecyclerItem> {
        val list = ArrayList<RecyclerItem>()

        games.forEach{
            var dsc = it.description.toString()
            if(dsc == "null")dsc=""
            var yr = it.yearPublished.toString()
            if(yr=="-1")yr="not aviable"
            list += RecyclerItem(it.gameId,"${it.name.toString()} (${yr})",dsc,it.thumbnail.toString())
        }

        return list
    }

}