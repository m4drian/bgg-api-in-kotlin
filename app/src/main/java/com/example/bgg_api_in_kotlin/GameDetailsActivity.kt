package com.example.bgg_api_in_kotlin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.*
import androidx.lifecycle.lifecycleScope
import com.example.bgg_api_in_kotlin.components.MyPreference
import com.example.bgg_api_in_kotlin.dataModel.Artist
import com.example.bgg_api_in_kotlin.dataModel.Designer
import com.example.bgg_api_in_kotlin.dataModel.XmlItem
import com.example.bgg_api_in_kotlin.dataModel.relations.BoardGameArtistCrossRef
import com.example.bgg_api_in_kotlin.dataModel.relations.BoardGameDesignerCrossRef
import com.example.bgg_api_in_kotlin.dbHelper.BoardGameDao
import com.example.bgg_api_in_kotlin.dbHelper.BoardGameDatabase
import com.example.bgg_api_in_kotlin.dbHelper.LocationDao
import kotlinx.android.synthetic.main.activity_game_details.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class GameDetailsActivity : AppCompatActivity() {

    private lateinit var xmlList : List<XmlItem>
    private  var gameId : Int = -1

    private lateinit var daoBgg : BoardGameDao

    private lateinit var daoLoc : LocationDao

    private lateinit var txtLoc : Spinner

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_details)

        val buttonBack = findViewById<Button>(R.id.buttonBackDetails)
        val buttonRanking = findViewById<Button>(R.id.buttonRanking)

        val txtId = findViewById<TextView>(R.id.ed1)
        val txtType = findViewById<TextView>(R.id.ed2)
        val txtName = findViewById<EditText>(R.id.ed3)
        val txtOgName = findViewById<EditText>(R.id.ed4)
        val txtDsc = findViewById<EditText>(R.id.ed5)
        val txtYr = findViewById<EditText>(R.id.ed6)
        val txtRank = findViewById<TextView>(R.id.ed7)
        val txtCom = findViewById<EditText>(R.id.ed8)
        val txtBuy = findViewById<EditText>(R.id.ed9)
        val txtRetail = findViewById<EditText>(R.id.ed10)
        val txtUpc = findViewById<EditText>(R.id.ed11)
        val txtProduction = findViewById<EditText>(R.id.ed12)
        val txtArtists = findViewById<EditText>(R.id.ed14)
        val txtDesigners = findViewById<EditText>(R.id.ed15)
        val txtEspansions = findViewById<TextView>(R.id.bggExpansions)
        txtLoc = findViewById<Spinner>(R.id.spinner)

        gameId = intent.getIntExtra("gameid",-1)
        val myPref = MyPreference(this)

        txtId.setText(gameId.toString())
        lifecycleScope.launch {
            daoBgg = BoardGameDatabase.getDatabase(this@GameDetailsActivity).boardGameDao()
            daoLoc = BoardGameDatabase.getDatabase(this@GameDetailsActivity).locationDao()
            val bggId2 = daoBgg.getGameWithId(gameId)
            if(bggId2!=null)
                if(bggId2.bggId > 0) myPref.setId(bggId2.bggId) else myPref.setId(-1)

            val locs = daoLoc.getLocation()
            var locations = ArrayList<String>()
            locations.add("")
            locs.forEach { locations.add(it.id.toString()+":"+it.name.toString()) }
            txtLoc.adapter = ArrayAdapter<String>(this@GameDetailsActivity,android.R.layout.simple_list_item_1,locations)


            txtLoc.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                }
                override fun onNothingSelected(parent: AdapterView<*>?) {
                }
            }
        }

        var xmlTask : MainActivity.DownloadXml

        GlobalScope.launch( context = Dispatchers.IO){

            daoBgg = BoardGameDatabase.getDatabase(this@GameDetailsActivity).boardGameDao()
            val bggId = daoBgg.getGameWithId(gameId)
            if(bggId!=null)
                if(bggId.bggId > 0) myPref.setId(bggId.bggId)else myPref.setId(-1)

            if(bggId != null) {
                if (bggId.bggId > 0) {

                    val collection =
                        "https://www.boardgamegeek.com/xmlapi2/thing?id=${bggId.bggId}&stats=1"
                    xmlTask = MainActivity.DownloadXml(this@GameDetailsActivity, collection, 3)
                    xmlTask.execute()

                    lifecycleScope.launch {

                        //add details to database

                        xmlList = xmlTask.get()

                        val xmlFirst = xmlList.first()

                        if(bggId.originalName.toString().isNullOrBlank())daoBgg.updateGameOgName(gameId,xmlFirst.originalName.toString())
                        if(bggId.description.toString().isNullOrBlank()||bggId.description.toString()=="null")daoBgg.updateGameDesc(gameId,xmlFirst.description.toString())
                        val tmpYear = xmlFirst.yearPublished.toString()
                        var year: Int = -1
                        if (!tmpYear.isNullOrBlank()) {
                            year = tmpYear.toInt()
                        }
                        if(bggId.yearPublished.toString().isNullOrBlank()||bggId.yearPublished == -1)daoBgg.updateGameYear(gameId,year)
                        if(bggId.comment.toString().isNullOrBlank()||bggId.description.toString()=="null")daoBgg.updateGameCom(gameId,xmlFirst.comment.toString())
                        //firstItem.artist
                        val artists = xmlFirst.artist.toString().split(";").toTypedArray()
                        artists.forEach {
                            if (it != "null" && it.isNotBlank()) {
                                daoBgg.addArtist(Artist(it))
                                daoBgg.addArtistGameCrossRef(BoardGameArtistCrossRef(gameId, it))
                            }
                        }
                        //firstItem.designer
                        val designers = xmlFirst.designer.toString().split(";").toTypedArray()
                        designers.forEach {
                            if (it != "null" && it.isNotBlank()) {
                                daoBgg.addDesigner(Designer(it))
                                daoBgg.addDesignerGameCrossRef(BoardGameDesignerCrossRef(gameId, it))
                            }
                        }
                        val bggId2 = daoBgg.getGameWithId(gameId)
                        if(bggId2 != null) {

                            txtType.setTextIsSelectable(false)
                            txtType.setText(xmlFirst.type.toString())
                            txtName.setText(bggId2.name.toString())
                            txtOgName.setText(bggId2.originalName.toString())
                            val tmpDsc = bggId2.description.toString()
                            if (tmpDsc != "null") txtDsc.setText(tmpDsc)
                            txtYr.setText(bggId2.yearPublished.toString())
                            txtRank.setText(bggId2.rank.toString())
                            val tmpCom = bggId2.comment.toString()
                            if (tmpCom != "null") txtCom.setText(tmpCom)
                            val tmpB = bggId2.buy.toString()
                            if (tmpB != "null") txtBuy.setText(bggId2.buy.toString())
                            val tmpS = bggId2.suggestedPrice.toString()
                            if (tmpS != "null") txtRetail.setText(bggId2.suggestedPrice.toString())
                            val tmpUpc = bggId2.eanUpcCode.toString()
                            if (tmpUpc != "null") txtUpc.setText(bggId2.eanUpcCode.toString())
                            val tmpP = bggId2.productionCode.toString()
                            if (tmpP != "null") txtProduction.setText(bggId2.productionCode.toString())

                            txtArtists.setTextIsSelectable(false)
                            txtDesigners.setTextIsSelectable(false)
                            txtArtists.setText(xmlFirst.artist.toString().replace(';', '\n'))
                            txtDesigners.setText(xmlFirst.designer.toString().replace(';', '\n'))

                            val tmpExp = xmlFirst.expansion.toString().replace(';', '\n')
                            if (tmpExp.isBlank()) {
                                txtEspansions.text = "No expansions aviable"
                            } else {
                                txtEspansions.text = tmpExp
                            }
                        }
                    }
                }
                else
                {

                    txtType.setTextIsSelectable(false)
                    txtType.setText(bggId.type.toString())
                    txtName.setText(bggId.name.toString())
                    txtOgName.setText(bggId.originalName.toString())
                    txtDsc.setText(bggId.description.toString())
                    txtYr.setText(bggId.yearPublished.toString())
                    txtRank.setText("Can't be ranked")
                    txtCom.setText(bggId.comment.toString())
                    txtBuy.setText(bggId.buy.toString())
                    txtRetail.setText(bggId.suggestedPrice.toString())
                    txtUpc.setText(bggId.eanUpcCode.toString())
                    txtProduction.setText(bggId.productionCode.toString())
                    txtEspansions.text = "No expansions aviable"

                    txtArtists.setTextIsSelectable(false)
                    txtDesigners.setTextIsSelectable(false)
                    txtArtists.setText("")
                    txtDesigners.setText("")


                }
            }
        }

        buttonBack.setOnClickListener{
            Handler(Looper.getMainLooper()).postDelayed({
                startActivity(Intent(this@GameDetailsActivity, MainActivity::class.java))
                finish()
            },100)
        }


        val intentRanking = Intent(this@GameDetailsActivity, GameRankingActivity::class.java)

        buttonRanking.setOnClickListener{
            intentRanking.putExtra("gameid",gameId)
            Handler(Looper.getMainLooper()).postDelayed({
                startActivity(intentRanking)
                finish()
            },100)
        }

        buttonConfirmDetails.setOnClickListener{
            if(!txtName.text.toString().isNullOrBlank()) {
                lifecycleScope.launch {
                    daoBgg = BoardGameDatabase.getDatabase(this@GameDetailsActivity).boardGameDao()
                    val bg = daoBgg.getGameWithId(gameId)

                    if (bg != null) {

                        var num: Int = 0
                        val selectedLoc = txtLoc.selectedItem.toString().split(':')
                        if (!selectedLoc.isNullOrEmpty()) {
                            val isLocNum = selectedLoc.first()
                            if (isLocNum.isNotBlank()) num = isLocNum.toInt()
                            else num = 0
                        } else {
                            num = 0
                        }

                        val tmpYear = txtYr.text.toString()
                        var year: Int = -1
                        if (!tmpYear.isNullOrBlank()) {
                            year = tmpYear.toInt()
                        }
                        daoBgg.updateGameWithId(
                                gameId,
                                txtName.text.toString(),
                                txtOgName.text.toString(),
                                txtDsc.text.toString(),
                                year,
                                txtCom.text.toString(),
                                txtBuy.text.toString(),
                                txtRetail.text.toString(),
                                txtUpc.text.toString(),
                                txtProduction.text.toString(),
                                num
                        )

                        Toast.makeText(
                            applicationContext,
                            "Game Updated!", Toast.LENGTH_SHORT
                        ).show()



                    }
                }
            }
            else
            {
                Toast.makeText(
                    applicationContext,
                    "Game needs a name!", Toast.LENGTH_SHORT
                ).show()
            }

        }
    }
}