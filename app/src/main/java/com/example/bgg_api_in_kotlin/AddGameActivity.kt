package com.example.bgg_api_in_kotlin

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.icu.text.SimpleDateFormat
import android.icu.util.Calendar
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.example.bgg_api_in_kotlin.components.MyPreference
import com.example.bgg_api_in_kotlin.dataModel.*
import com.example.bgg_api_in_kotlin.dataModel.relations.BoardGameArtistCrossRef
import com.example.bgg_api_in_kotlin.dataModel.relations.BoardGameDesignerCrossRef
import com.example.bgg_api_in_kotlin.dbHelper.BoardGameDao
import com.example.bgg_api_in_kotlin.dbHelper.BoardGameDatabase
import com.example.bgg_api_in_kotlin.dbHelper.BoardGameViewModel
import com.example.bgg_api_in_kotlin.dbHelper.LocationDao
import kotlinx.android.synthetic.main.activity_addgame.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.Month
import java.time.temporal.ChronoUnit
import java.util.*
import kotlin.collections.ArrayList

class AddGameActivity : AppCompatActivity() {

    private lateinit var viewModelBgg: BoardGameViewModel

    private lateinit var daoBgg : BoardGameDao

    private lateinit var daoLoc : LocationDao

    private lateinit var xmlList : List<XmlItem>

    private lateinit var xmlTask : MainActivity.DownloadXml

    private lateinit var bgs : ArrayList<BoardGame>

    private lateinit var txtLoc : Spinner

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_addgame)

        val txtName = findViewById<EditText>(R.id.ed1)
        txtName.requestFocus()
        val txtOgName = findViewById<EditText>(R.id.ed2)
        val txtDsc = findViewById<EditText>(R.id.ed3)
        val txtYr = findViewById<EditText>(R.id.ed4)
        val txtCom = findViewById<EditText>(R.id.ed5)
        val txtBuy = findViewById<EditText>(R.id.ed6)
        val txtRetail = findViewById<EditText>(R.id.ed7)
        val txtUpc = findViewById<EditText>(R.id.ed8)
        val txtProduction = findViewById<EditText>(R.id.ed9)
        txtLoc = findViewById<Spinner>(R.id.spinner)

        lifecycleScope.launch {
            daoLoc = BoardGameDatabase.getDatabase(this@AddGameActivity).locationDao()
            val locs = daoLoc.getLocation()
            var locations = ArrayList<String>()
            locations.add("")
            locs.forEach { locations.add(it.id.toString()+":"+it.name.toString()) }
            txtLoc.adapter = ArrayAdapter<String>(this@AddGameActivity,android.R.layout.simple_list_item_1,locations)

            txtLoc.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                }
                override fun onNothingSelected(parent: AdapterView<*>?) {
                }
            }
        }

        val buttonOk = findViewById<Button>(R.id.buttonOkAddGame)
        val buttonCheckName =  findViewById<Button>(R.id.buttonCheckName)
        val buttonAddGameConfirm =  findViewById<Button>(R.id.buttonAddGameConfirm)

        buttonOk.setOnClickListener{
            Handler(Looper.getMainLooper()).postDelayed({
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            },100)
        }

        buttonCheckName.setOnClickListener{

            val name = txtName.text.toString()
            if(!name.isBlank()) {

                val search = "https://www.boardgamegeek.com/xmlapi2/search?query=$name&type=boardgame"
                xmlTask = MainActivity.DownloadXml(this@AddGameActivity, search, 1)

                GlobalScope.launch( context = Dispatchers.Main){
                    xmlTask.execute()
                    xmlList = xmlTask.get()

                    val builder = AlertDialog.Builder(this@AddGameActivity)
                    //builder.setTitle("Androidly Alert")
                    //builder.setMessage("We have a message")


                    if(!xmlList.isNullOrEmpty()) {
                        val firstItem = xmlList.first()

                        builder.setTitle("BGG search")
                        builder.setMessage("Is this the game you're looking for?: \n${firstItem.originalName.toString()}")

                        builder.setPositiveButton("Insert") { dialog, which ->

                            insertGame(firstItem.bggid)

                            Toast.makeText(applicationContext,
                                "Game Inserted", Toast.LENGTH_SHORT).show()
                        }

                        builder.setNegativeButton("Cancel") { dialog, which ->
                        }

                        builder.show()


                    }
                    else{

                        Toast.makeText(applicationContext,
                            "Nothing found", Toast.LENGTH_SHORT).show()
                    }

                }
            }

        }

        buttonAddGameConfirm.setOnClickListener{

            if(!txtName.text.toString().isNullOrBlank()) {
                var ogName = txtOgName.text.toString()
                if(!ogName.isNullOrBlank())ogName = txtName.text.toString()

                var num: Int? = null
                val selectedLoc = txtLoc.selectedItem.toString().split(':')
                if (!selectedLoc.isNullOrEmpty()) {
                    val isLocNum = selectedLoc.first()
                    if (isLocNum.isNotBlank()) num = isLocNum.toInt()
                    else num = null
                } else {
                    num = null
                }

                val tmpYear = txtYr.text.toString()
                var year: Int? = -1
                if (!tmpYear.isNullOrBlank()) {
                    year = tmpYear.toInt()
                }

                lifecycleScope.launch {
                    daoBgg = BoardGameDatabase.getDatabase(this@AddGameActivity).boardGameDao()
                    daoBgg.addBoardGame(
                        BoardGame(
                            0,
                            -1,
                            "boardgame",
                            "",
                            txtName.text.toString(),
                            ogName,
                            txtDsc.text.toString(),
                            year,
                            -1,
                            txtCom.text.toString(),
                            txtBuy.text.toString(),
                            txtRetail.text.toString(),
                            txtUpc.text.toString(),
                            txtProduction.text.toString(),
                            num
                        )
                    )

                }

                Toast.makeText(
                    applicationContext,
                    "Game inserted", Toast.LENGTH_SHORT
                ).show()
            }
            else {
                Toast.makeText(
                    applicationContext,
                    "Game needs a name!", Toast.LENGTH_SHORT
                ).show()
            }

        }

        val myPref = MyPreference(this)
        val yearDefault = myPref.getDate()

    }

    private fun insertGame(bggId : Int)
    {

        val details = "https://www.boardgamegeek.com/xmlapi2/thing?id=$bggId&stats=1"
        xmlTask = MainActivity.DownloadXml(this@AddGameActivity, details, 3)
        xmlTask.execute()
        xmlList = xmlTask.get()
        if(!xmlList.isNullOrEmpty()) {
            val firstItem = xmlList.first()

            lifecycleScope.launch {
                daoBgg = BoardGameDatabase.getDatabase(this@AddGameActivity).boardGameDao()

                val isRank = firstItem.rank
                val tRank : Int = if(isRank!=null) {
                    if(isRank.isNotBlank() && isRank.toIntOrNull()!=null)
                        isRank.toInt()
                    else
                        -1
                } else {
                    -1
                }

                if(!daoBgg.isGameExists(bggId)) {

                    daoBgg.addBoardGame(
                        BoardGame(
                            0,
                            firstItem.bggid,
                            firstItem.type,
                            firstItem.thumbnail,
                            firstItem.name,
                            firstItem.originalName,
                            firstItem.description,
                            firstItem.yearPublished,
                            tRank,
                            firstItem.comment,
                            null,
                            null,
                            null,
                            null,
                            null
                        )
                    )

                    //rank
                    val date = Calendar.getInstance().time
                    val strDate = SimpleDateFormat("yyyy/MM/dd HH:mm", Locale.getDefault()).format(date)

                    if(tRank!=-1) {
                        daoBgg.addRank(RankHistory(0, strDate, tRank, firstItem.bggid))
                    }

                    //firstItem.artist
                    val artists = firstItem.artist.toString().split(";").toTypedArray()
                    artists.forEach {
                        if (it != "null" && it.isNotBlank()) {
                            daoBgg.addArtist(Artist(it))
                            daoBgg.addArtistGameCrossRef(BoardGameArtistCrossRef(bggId, it))
                        }
                    }
                    //firstItem.designer
                    val designers = firstItem.designer.toString().split(";").toTypedArray()
                    designers.forEach {
                        if (it != "null" && it.isNotBlank()) {
                            daoBgg.addDesigner(Designer(it))
                            daoBgg.addDesignerGameCrossRef(BoardGameDesignerCrossRef(bggId, it))
                        }
                    }
                }

            }




        }

    }
}