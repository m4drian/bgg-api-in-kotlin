package com.example.bgg_api_in_kotlin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Button
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bgg_api_in_kotlin.components.MyPreference
import com.example.bgg_api_in_kotlin.components.RecyclerEditLocAdapter
import com.example.bgg_api_in_kotlin.components.RecyclerLocation
import com.example.bgg_api_in_kotlin.dataModel.RankHistory
import com.example.bgg_api_in_kotlin.dbHelper.BoardGameDao
import com.example.bgg_api_in_kotlin.dbHelper.BoardGameDatabase
import kotlinx.coroutines.launch

class GameRankingActivity : AppCompatActivity() {

    private lateinit var daoGames : BoardGameDao

    private var adapter : RecyclerEditLocAdapter = RecyclerEditLocAdapter()

    private lateinit var recyclerViewItems : List<RecyclerLocation>
    private lateinit var recyclerRanks : RecyclerView

    private var gameId : Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_ranking)

        val intentDetails = Intent(this@GameRankingActivity, GameDetailsActivity::class.java)

        val buttonBack = findViewById<Button>(R.id.buttonRankingBack)

        gameId = intent.getIntExtra("gameid",-1)

        val myPref = MyPreference(this)
        val gameid2 = myPref.getId()

        Toast.makeText(
            applicationContext,
            gameid2.toString(), Toast.LENGTH_SHORT
        ).show()

        recyclerRanks = findViewById<RecyclerView>(R.id.recyclerRanking)

        lifecycleScope.launch() {
            daoGames = BoardGameDatabase.getDatabase(this@GameRankingActivity).boardGameDao()


                val daoRanks = daoGames.getRankById(gameid2)
                recyclerViewItems = generateRecyclerViewItems(daoRanks)

                adapter.updateData(recyclerViewItems)
                recyclerRanks.adapter = adapter
                recyclerRanks.layoutManager = LinearLayoutManager(this@GameRankingActivity)
                //Toast.makeText(this@EditLocActivity,locId.toString(),Toast.LENGTH_SHORT).show()

        }

        buttonBack.setOnClickListener{
            intentDetails.putExtra("gameid",gameId)
            Handler(Looper.getMainLooper()).postDelayed({
                startActivity(intentDetails)
                finish()
            },100)
        }

    }

    private fun generateRecyclerViewItems(ranks:List<RankHistory>?): List<RecyclerLocation> {
        val list = ArrayList<RecyclerLocation>()

        if(!ranks.isNullOrEmpty())
        {

            ranks.forEach{//it.bggId.toString()
                list += RecyclerLocation("",it.date.toString(),"rank: ${it.rank.toString()}")
            }

        }
        else{
            val item = RecyclerLocation("","game not ranked", "")
            list += item
        }

        return list
    }
}