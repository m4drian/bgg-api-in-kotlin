package com.example.bgg_api_in_kotlin.components

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.bgg_api_in_kotlin.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.recycler_item.view.*

class RecyclerAdapter () :
    RecyclerView.Adapter<RecyclerAdapter.MainViewHolder>() {

    private lateinit var mListener : OnItemClickListener
    private lateinit var exampleList: List<RecyclerItem>

    interface OnItemClickListener{
        fun onItemClick(position : Int)

        fun onLongItemClick(position : Int)

    }

    fun setOnItemClickListener(listener : RecyclerAdapter.OnItemClickListener){
        mListener = listener
    }

    fun updateData(newList : List<RecyclerItem>){
        exampleList = newList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.recycler_item,
            parent, false
        )
        return MainViewHolder(itemView,mListener)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val currentItem = exampleList[position]
        holder.itemId.text = currentItem.id.toString()
        holder.textView1.text = currentItem.text1
        holder.textView2.text = currentItem.text2
        if(currentItem.url.isNotBlank()) {
            Picasso.get().load(currentItem.url).into(holder.imageView)
        }
    }

    override fun getItemCount() = exampleList.size

    class MainViewHolder(itemView: View, listener: RecyclerAdapter.OnItemClickListener) : RecyclerView.ViewHolder(itemView) {

        val textView1: TextView = itemView.text_view_1  //synthetic
        val textView2: TextView = itemView.text_view_2
        val itemId: TextView = itemView.textId
        val imageView: ImageView = itemView.imageView
        //val textView2: TextView = itemView.findViewById(R.id.text_view_2)

        init{

            itemView.setOnClickListener {
                listener.onItemClick(adapterPosition)
            }


            itemView.setOnLongClickListener {
                listener.onLongItemClick(adapterPosition)
                return@setOnLongClickListener true
            }
        }
    }
}

