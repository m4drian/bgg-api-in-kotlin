package com.example.bgg_api_in_kotlin.components

import android.util.Xml
import com.example.bgg_api_in_kotlin.dataModel.XmlItem
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

/* Kod na podstawie oficjalnego poradnika do xml do android studio,
 orginał dostępny pod źródłem: https://developer.android.com/training/basics/network-ops/xml */

private val ns: String? = null

class XmlParser {

    @Throws(XmlPullParserException::class, IOException::class)
    fun parse(inputStream: InputStream, which: Int?): List<XmlItem> {
        inputStream.use { inputStream ->
            val parser: XmlPullParser = Xml.newPullParser()
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)
            parser.setInput(inputStream, null)
            parser.nextTag()
            return readItems(parser,which)
        }
    }


    @Throws(XmlPullParserException::class, IOException::class)
    private fun readItems(parser: XmlPullParser,which: Int?): List<XmlItem> {
        val xmlItems = mutableListOf<XmlItem>()
        parser.require(XmlPullParser.START_TAG, ns, "items")
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }

            //--------------------------------------------1-search,2-collection,else-details
            if (parser.name == "item") {
                when(which) {
                    1 -> xmlItems.add(readSearchItem(parser))
                    2 -> xmlItems.add(readCollectionItem(parser))
                    else -> xmlItems.add(readDetailsItem(parser))
                }
            } else {
                 skip(parser)
            }
        }
        return xmlItems
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun readSearchItem(parser: XmlPullParser): XmlItem {
        parser.require(XmlPullParser.START_TAG, ns, "item")
        val bggId: Int = parser.getAttributeValue(null, "id").toInt()
        var name: String = ""
        var originalName: String = ""
        var yearPublished: Int = -1
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            when (parser.name) {
                "name" -> originalName = readName(parser)
                "yearpublished" -> yearPublished = readAttribute("yearpublished","value",parser).toInt()
                else -> skip(parser)
            }
        }
        name = originalName;
        return XmlItem(bggId,name, originalName, yearPublished)
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun readCollectionItem(parser: XmlPullParser): XmlItem {
        parser.require(XmlPullParser.START_TAG, ns, "item")
        val bggId: Int = parser.getAttributeValue(null, "objectid").toInt()
        val type: String = parser.getAttributeValue(null, "subtype")
        var name: String = ""
        var originalName: String = ""
        var yearPublished: Int = -1
        var thumbnail: String = ""
        var rank: String = ""
        var comment: String = ""
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            when (parser.name) {
                "name" -> originalName = readInside("name",parser)
                "yearpublished" -> yearPublished = readInside("yearpublished",parser).toInt()
                "thumbnail" -> thumbnail = readInside("thumbnail",parser)
                "stats" -> rank = readRankCollection(parser)
                "comment" -> comment = readInside("comment",parser)
                else -> skip(parser)
            }
        }
        name = originalName;
        return XmlItem(bggId, type, name,originalName, yearPublished,thumbnail,rank,comment)
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun readDetailsItem(parser: XmlPullParser): XmlItem {
        parser.require(XmlPullParser.START_TAG, ns, "item")
        var nameCount = 0
        val bggid: Int = parser.getAttributeValue(null, "id").toInt()
        val type: String = parser.getAttributeValue(null, "type")
        var name: String = ""
        var originalName: String = ""
        var description: String = ""
        var yearPublished: Int = -1
        var thumbnail: String = ""
        var rank: String = ""
        var designer:String = ""
        var artist:String = ""
        var expansion:String = ""
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            when (parser.name) {
                "thumbnail" -> thumbnail = readInside("thumbnail",parser)
                "name" -> {
                    val relType = parser.getAttributeValue(null, "type")
                    if(relType == "primary") {
                        originalName = readAttribute("name", "value", parser)
                    }else{skip(parser)}
                }
                "description" -> description = readInside("description",parser)
                "yearpublished" -> yearPublished = readAttribute("yearpublished","value",parser).toInt()
                "stats" -> rank = readRankCollection(parser)
                "link" -> {
                    val relType = parser.getAttributeValue(null, "type")
                    if(relType=="boardgamedesigner") {
                        designer += readAttribute("link", "value", parser) + ";"
                    }
                    else if(relType=="boardgameartist") {
                        artist += readAttribute("link", "value", parser) + ";"
                    }
                    else if(relType=="boardgameexpansion") {
                        expansion += readAttribute("link", "value", parser) + ";"
                    }
                    else{
                        skip(parser)
                    }
                }
                "statistics" -> rank = readRankDetails(parser)
                else -> skip(parser)
            }
        }
        name = originalName
        return XmlItem(bggid,type , thumbnail, name, originalName,description, yearPublished,designer,artist,rank,expansion)
    }

    @Throws(IOException::class, XmlPullParserException::class)
    private fun readName(parser: XmlPullParser): String {
        var value = ""
        val relType = parser.getAttributeValue(null, "type")
        if(relType == "primary" || relType == "alternate") {
            parser.require(XmlPullParser.START_TAG, ns, "name")
            value = parser.getAttributeValue(null, "value")
            parser.nextTag()
            parser.require(XmlPullParser.END_TAG, ns, "name")
        }
        else{skip(parser)}
        return value
    }

    @Throws(IOException::class, XmlPullParserException::class)
    private fun readRankDetails(parser: XmlPullParser): String {
        var value : String = ""

        parser.require(XmlPullParser.START_TAG, ns, "statistics")
        parser.nextTag()
        parser.require(XmlPullParser.START_TAG, ns, "ratings")
        skipAmount(parser,7)//user_rated //average //bayesaverage //ranks
        parser.require(XmlPullParser.START_TAG, ns, "ranks")

        parser.nextTag()   //rank
        parser.require(XmlPullParser.START_TAG, ns, "rank")

        value = parser.getAttributeValue(null,"value")

        parser.nextTag()
        parser.require(XmlPullParser.END_TAG, ns, "rank")

        return value
    }

    @Throws(IOException::class, XmlPullParserException::class)
    private fun readRankCollection(parser: XmlPullParser): String {//TODO:properly go through rank

        var value : String = ""

        parser.require(XmlPullParser.START_TAG, ns, "stats")
        parser.nextTag()
        parser.require(XmlPullParser.START_TAG, ns, "rating")
        skipAmount(parser,11)//11 for ranks
        parser.require(XmlPullParser.START_TAG, ns, "ranks")
        //println(parser.name)
        
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            else
            {
                val isGeneralRanking = parser.getAttributeValue(null,"id")
                if(isGeneralRanking=="1") {
                    value = parser.getAttributeValue(null,"value")
                }
                skip(parser)
            }
        }

        parser.require(XmlPullParser.END_TAG, ns, "ranks")
        parser.nextTag()
        parser.require(XmlPullParser.END_TAG, ns, "rating")
        parser.nextTag()
        parser.require(XmlPullParser.END_TAG, ns, "stats")


        return value
    }

    @Throws(IOException::class, XmlPullParserException::class)
    private fun readAttribute(tag:String,attribute:String,parser: XmlPullParser): String {
        parser.require(XmlPullParser.START_TAG, ns, tag)
        val value = parser.getAttributeValue(null, attribute)
        parser.nextTag()
        parser.require(XmlPullParser.END_TAG, ns, tag)
        return value
    }

    @Throws(IOException::class, XmlPullParserException::class)
    private fun readInside(tag:String,parser: XmlPullParser): String {
        parser.require(XmlPullParser.START_TAG, ns, tag)
        val inside = readText(parser)
        parser.require(XmlPullParser.END_TAG, ns, tag)
        return inside
    }


    // extract text values
    @Throws(IOException::class, XmlPullParserException::class)
    private fun readText(parser: XmlPullParser): String {
        var result = ""
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.text
            parser.nextTag()
        }
        return result
    }


    @Throws(XmlPullParserException::class, IOException::class)
    private fun skip(parser: XmlPullParser) {
        if (parser.eventType != XmlPullParser.START_TAG) {
            throw IllegalStateException()
        }
        var depth = 1
        while (depth != 0) {
            when (parser.next()) {
                XmlPullParser.END_TAG -> depth--
                XmlPullParser.START_TAG -> depth++
            }
        }
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun skipAmount(parser: XmlPullParser, amount: Int) {
        var i = amount
        while(i>0)
        {
            i--
            parser.nextTag()
        }
    }


    @Throws(XmlPullParserException::class, IOException::class)
    fun loadXmlFromNetwork(urlString: String, which: Int?): List<XmlItem> {

        return downloadUrl(urlString)?.use { stream ->
            XmlParser().parse(stream, which)
        } ?: emptyList()
    }

    //sets up a connection and gets an input stream.
    @Throws(IOException::class)
    private fun downloadUrl(urlString: String): InputStream? {
        val url = URL(urlString)
        return (url.openConnection() as? HttpURLConnection)?.run {
            readTimeout = 10000
            connectTimeout = 15000
            requestMethod = "GET"
            doInput = true
            // Starts the query
            connect()
            inputStream
        }
    }

}

