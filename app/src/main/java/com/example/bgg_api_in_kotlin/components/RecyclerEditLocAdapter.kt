package com.example.bgg_api_in_kotlin.components

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.bgg_api_in_kotlin.R
import kotlinx.android.synthetic.main.recycler_item2.view.*

class RecyclerEditLocAdapter () :
    RecyclerView.Adapter<RecyclerEditLocAdapter.LocationViewHolder>() {

    private lateinit var exampleList: List<RecyclerLocation>



    fun updateData(newList : List<RecyclerLocation>){
        exampleList = newList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocationViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.recycler_item2,
            parent, false
        )
        return LocationViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: LocationViewHolder, position: Int) {
        val currentItem = exampleList[position]
        holder.textView1.text = currentItem.text1
        holder.textView2.text = currentItem.text2
        holder.textView0.text = currentItem.text0
    }

    override fun getItemCount() = exampleList.size

    class LocationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val textView1: TextView = itemView.txt_loc1  //synthetic
        val textView2: TextView = itemView.txt_loc2
        val textView0: TextView = itemView.txt_loc0

    }


}

