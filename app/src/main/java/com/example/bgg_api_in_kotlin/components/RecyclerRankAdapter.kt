package com.example.bgg_api_in_kotlin.components

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.bgg_api_in_kotlin.R
import kotlinx.android.synthetic.main.recycler_item3.view.*

class RecyclerRankAdapter () :
    RecyclerView.Adapter<RecyclerRankAdapter.RankViewHolder>() {

    private lateinit var exampleList: List<RecyclerLocation>



    fun updateData(newList : List<RecyclerLocation>){
        exampleList = newList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RankViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.recycler_item2,
            parent, false
        )
        return RankViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: RankViewHolder, position: Int) {
        val currentItem = exampleList[position]
        holder.textView1.text = currentItem.text1
        //holder.textView2 = currentItem.text2
        holder.textView0.text = currentItem.text0
    }

    override fun getItemCount() = exampleList.size

    class RankViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val textView1: TextView = itemView.text1  //synthetic
        val textView2: String = ""
        val textView0: TextView = itemView.text0

    }


}

