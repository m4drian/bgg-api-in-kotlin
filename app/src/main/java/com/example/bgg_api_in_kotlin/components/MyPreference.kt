package com.example.bgg_api_in_kotlin.components

import android.content.Context
import android.icu.util.Calendar

class MyPreference(context: Context){

    val PREFERENCE_NAME = "SharedPreference1"
    val PREFERENCE_DATE = "SelectedDate"
    val BGG_NICK = "SelectedNick"
    val GAME_ID = "GameId"

    val preference = context.getSharedPreferences(PREFERENCE_NAME,Context.MODE_PRIVATE)

    fun getDate() : Int{
        return preference.getInt(PREFERENCE_DATE, Calendar.getInstance().get(Calendar.YEAR))
    }

    fun setDate(date:Int){
        val editor = preference.edit()
        editor.putInt(PREFERENCE_DATE, date)
        editor.apply()
    }

    fun getNick() : String? {
        return preference.getString(BGG_NICK, "")
    }

    fun setNick(nick : String){
        val editor = preference.edit()
        editor.putString(BGG_NICK, nick)
        editor.apply()
    }

    fun getId() : Int {
        return preference.getInt(GAME_ID, -1)
    }

    fun setId(id : Int){
        val editor = preference.edit()
        editor.putInt(GAME_ID, id)
        editor.apply()
    }

}