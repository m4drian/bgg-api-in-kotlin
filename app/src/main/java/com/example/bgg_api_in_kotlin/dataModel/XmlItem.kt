package com.example.bgg_api_in_kotlin.dataModel

class XmlItem{
    //var id:Int = 0
    var bggid:Int = -1//int

    var type:String? = null
    var thumbnail:String? = null
    var name:String? = null
    var originalName:String? = null
    var description:String? = null
    var yearPublished:Int = -1//int
    var designer:String? = null
    var artist:String? = null
    var rank:String? = null//int
    var comment:String? = null
    var expansion:String? = null


    constructor(){
        this.bggid=-1
        this.type="type"
        this.thumbnail="thumbnail"
        this.name="name"
        this.originalName="originalName"
        this.description="description"
        this.yearPublished=-1

        this.designer="designer"
        this.artist="artist"

        this.rank="rank"
        this.comment="comment"
    }

    constructor(name: String) {
        this.bggid=-1
        this.name=name
    }

    //for search
    constructor(bggId:Int, name:String, originalName:String, year:Int)
    {
        //this.id=id
        this.bggid=bggId
        this.name=name
        this.originalName=originalName
        this.yearPublished=year
    }

    //for players collection
    constructor(bggId:Int, type:String, name:String, originalName:String, year:Int, thumbnail:String, rank:String, comment:String)
    {
        //this.id=id
        this.bggid=bggId
        this.type=type
        this.name=name
        this.originalName=originalName
        this.yearPublished=year
        this.thumbnail=thumbnail
        this.rank=rank
        this.comment=comment
    }

    //for details
    constructor(bggId:Int, type:String, thumbnail:String, name:String, originalName:String, description:String, year:Int,
                designer:String, artist:String, rank:String, expansion:String)
    {
        //this.id=id
        this.bggid=bggId
        this.type=type
        this.thumbnail=thumbnail
        this.name=name
        this.originalName=originalName
        this.description=description
        this.yearPublished=year

        this.designer=designer
        this.artist=artist

        this.rank=rank

        this.expansion = expansion
    }
}
