package com.example.bgg_api_in_kotlin.dataModel

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "artist_table")
data class Artist(
    @PrimaryKey(autoGenerate = false)
    val name:String
)