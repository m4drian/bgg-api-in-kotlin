package com.example.bgg_api_in_kotlin.dataModel.relations

import androidx.room.Entity

@Entity(tableName ="artist_crossref",primaryKeys = ["gameId","name"])
data class BoardGameArtistCrossRef (
    val gameId : Int,
    val name : String
)