package com.example.bgg_api_in_kotlin.dataModel.relations

import androidx.room.Embedded
import androidx.room.Relation
import com.example.bgg_api_in_kotlin.dataModel.BoardGame
import com.example.bgg_api_in_kotlin.dataModel.Location

data class LocationWithBoardGames (
    @Embedded val location: Location,
    @Relation(
        parentColumn = "id",
        entityColumn = "gameId"
    )
    val boardGames: List<BoardGame>
)