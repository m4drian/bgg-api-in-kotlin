package com.example.bgg_api_in_kotlin.dataModel

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "location_table")
data class Location(
    @PrimaryKey(autoGenerate = true)
    val id:Int,
    val name:String?,
    val description:String?
)