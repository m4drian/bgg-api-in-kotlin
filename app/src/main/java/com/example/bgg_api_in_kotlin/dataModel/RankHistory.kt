package com.example.bgg_api_in_kotlin.dataModel

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "rank_history_table")
data class RankHistory(
    @PrimaryKey(autoGenerate = true)
    val id:Int,
    val date:String,
    val rank:Int,
    //dbonly
    val bggId:Int
)