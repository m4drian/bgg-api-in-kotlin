package com.example.bgg_api_in_kotlin.dataModel.relations

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation
import com.example.bgg_api_in_kotlin.dataModel.Artist
import com.example.bgg_api_in_kotlin.dataModel.BoardGame

data class GameWithArtists (

    @Embedded
    val boardGame: BoardGame,
    @Relation(
        parentColumn = "gameId",
        entityColumn = "name",
        associateBy = Junction(BoardGameArtistCrossRef::class)
    )
    val designers: List<Artist>

    )