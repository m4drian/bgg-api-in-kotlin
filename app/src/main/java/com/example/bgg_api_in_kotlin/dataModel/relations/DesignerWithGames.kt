package com.example.bgg_api_in_kotlin.dataModel.relations

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation
import com.example.bgg_api_in_kotlin.dataModel.BoardGame
import com.example.bgg_api_in_kotlin.dataModel.Designer

data class DesignerWithGames (
    @Embedded
    val designer: Designer,
    @Relation(
        parentColumn = "name",
        entityColumn = "gameId",
        associateBy = Junction(BoardGameDesignerCrossRef::class)
    )
    val boardGames: List<BoardGame>
)