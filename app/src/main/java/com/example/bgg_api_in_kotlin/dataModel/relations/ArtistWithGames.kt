package com.example.bgg_api_in_kotlin.dataModel.relations

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation
import com.example.bgg_api_in_kotlin.dataModel.Artist
import com.example.bgg_api_in_kotlin.dataModel.BoardGame

data class ArtistWithGames (

    @Embedded
    val artist: Artist,
    @Relation(
        parentColumn = "name",
        entityColumn = "gameId",
        associateBy = Junction(BoardGameArtistCrossRef::class)
    )
    val boardGames: List<BoardGame>

    )