package com.example.bgg_api_in_kotlin.dataModel.relations

import androidx.room.Entity

@Entity(tableName ="designer_crossref",primaryKeys = ["gameId","name"])
data class BoardGameDesignerCrossRef (
    val gameId : Int,
    val name : String
    )