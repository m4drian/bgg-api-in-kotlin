package com.example.bgg_api_in_kotlin.dataModel

class PlayerScore {
    var id:Int = 0
    var name:String? = null
    var score:Int = 0

    constructor(){}

    constructor(id:Int,name:String,score:Int)
    {
        this.id=id
        this.name=name
        this.score=score
    }
}