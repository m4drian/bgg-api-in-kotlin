package com.example.bgg_api_in_kotlin.dataModel

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "board_game_table")
data class BoardGame (
    @PrimaryKey(autoGenerate = true)
    val gameId: Int,
    val bggId:Int,

    val type:String?,
    val thumbnail:String?,
    val name:String?,
    val originalName:String?,
    val description:String?,
    val yearPublished:Int?,
    val rank:Int?,
    val comment:String?,

    //database only
    val buy:String?,
    val suggestedPrice:String?,
    val eanUpcCode:String?,
    val productionCode:String?,
    val locationId:Int?
)