package com.example.bgg_api_in_kotlin.dataModel.relations

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation
import com.example.bgg_api_in_kotlin.dataModel.BoardGame
import com.example.bgg_api_in_kotlin.dataModel.Designer

data class GameWithDesigners (
    @Embedded
    val boardGame: BoardGame,
    @Relation(
        parentColumn = "gameId",
        entityColumn = "name",
        associateBy = Junction(BoardGameDesignerCrossRef::class)
    )
    val designers: List<Designer>
)