package com.example.bgg_api_in_kotlin.dataModel

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "designer_table")
data class Designer(
    @PrimaryKey(autoGenerate = false)
    val name:String
)